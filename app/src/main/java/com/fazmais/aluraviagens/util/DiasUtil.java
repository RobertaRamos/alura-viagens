package com.fazmais.aluraviagens.util;

import android.content.Context;

import com.fazmais.aluraviagens.R;

public class DiasUtil {

    public static String formtaDiasEmTexto(int dias, Context context) {
        return context.getResources().getQuantityString(R.plurals.numberOfItems, dias, dias);
    }
}
