package com.fazmais.aluraviagens.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.fazmais.aluraviagens.R;
import com.fazmais.aluraviagens.model.Pacote;
import com.fazmais.aluraviagens.util.MoedaUtil;

import java.math.BigDecimal;

import static com.fazmais.aluraviagens.ui.activity.PacoteActivityConstantes.CHAVE_PACOTE;

public class PagamentoActivity extends AppCompatActivity {

    public static final String TITULO_APPBAR = "Pagamento";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagamento);
        setTitle(TITULO_APPBAR);
        carregaPacoteRecebido();

    }

    private void carregaPacoteRecebido() {
        Intent intent = getIntent();
        if (intent.hasExtra(CHAVE_PACOTE)) {
            Pacote pacote = (Pacote) intent.getSerializableExtra(CHAVE_PACOTE);
            mostraPreco(pacote);
            configuraBotao(pacote);

        }
    }

    private void configuraBotao(Pacote pacote) {
        Button botaoFinalizaCompra = findViewById(R.id.pagamento_botao_finaliza_compra);
        botaoFinalizaCompra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vaiParaResumoCompra(pacote);

            }
        });
    }

    private void vaiParaResumoCompra(Pacote pacote) {
        EditText numeroDoCartao = findViewById(R.id.pagamento_numero_do_cartao);
        EditText mesCartao = findViewById(R.id.pagamento_mes_cartao);
        EditText anoCartao = findViewById(R.id.pagamento_ano_cartao);
        EditText cvcCartao = findViewById(R.id.pagamento_cvc_cartao);
        EditText nomeCartao = findViewById(R.id.pagamento_nome_cartao);

        if (numeroDoCartao.length()<12){
            numeroDoCartao.setError("Verifique o número do cartão");

        }
        if(mesCartao.length()<2){
            mesCartao.setError("Verifique o ano do cartão");

        }
        if(anoCartao.length()<2){
            anoCartao.setError("Verifique o ano do cartão");

        }
        if(cvcCartao.length()<3){
            cvcCartao.setError("Verifique o cvc do cartão");

        }

        if(nomeCartao.length()<5){
            nomeCartao.setError("Verifique o nome do titular do cartão");

        }

        if (numeroDoCartao.length() == 12 & mesCartao.length() == 2 & anoCartao.length() == 2
                & cvcCartao.length() == 3 & nomeCartao.length() >= 5) {
            Intent intent = new Intent(this, ResumoCompraActivity.class);
            intent.putExtra(CHAVE_PACOTE, pacote);
            startActivity(intent);
            finish();
        }

    }

    private void mostraPreco(Pacote pacote) {
        TextView preco = findViewById(R.id.pagamento_preco);
        BigDecimal valorDoPacote = pacote.getPreco();
        String precoConvertidoDoPacote = MoedaUtil.formataMoedaParaBrasileiro(valorDoPacote);
        preco.setText(precoConvertidoDoPacote);
    }

}

