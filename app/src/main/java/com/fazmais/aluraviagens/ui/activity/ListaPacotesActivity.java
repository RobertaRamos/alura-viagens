package com.fazmais.aluraviagens.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.fazmais.aluraviagens.R;
import com.fazmais.aluraviagens.dao.PacoteDao;
import com.fazmais.aluraviagens.model.Pacote;
import com.fazmais.aluraviagens.ui.OnItemClickListiner;
import com.fazmais.aluraviagens.ui.activity.recyclerview.ListaPacoteAdapterRecyclerView;

import java.util.List;

import static com.fazmais.aluraviagens.ui.activity.PacoteActivityConstantes.CHAVE_PACOTE;

public class ListaPacotesActivity extends AppCompatActivity {

    public static final String TITULO_APPBAR = "Pacotes";
    private List<Pacote> pacotes;
    private ListaPacoteAdapterRecyclerView adapterRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pacotes);
        setTitle(TITULO_APPBAR);
        configuraLista();
    }

    private void configuraLista() {
        RecyclerView listaDePacotes = findViewById(R.id.lista_pacotes_recyclerview);
//        listaDePacotes.setLayoutManager(new LinearLayoutManager(this)); - feito via XML
        pacotes = new PacoteDao().lista();
        adapterRecyclerView = new ListaPacoteAdapterRecyclerView(this, pacotes);
        listaDePacotes.setAdapter(adapterRecyclerView);
        adapterRecyclerView.setOnItemClickListiner(new OnItemClickListiner() {
            @Override
            public void onItemClick(int posicao) {
                vaiParaResumoPacote(posicao);

            }
        });

    }
    private void vaiParaResumoPacote(int posicao) {
        Intent intent = new Intent(this, ResumoPacoteActivity.class);
        intent.putExtra(CHAVE_PACOTE, pacotes.get(posicao));
        startActivity(intent);
    }


}