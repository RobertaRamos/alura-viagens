package com.fazmais.aluraviagens.ui.activity.recyclerview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fazmais.aluraviagens.R;
import com.fazmais.aluraviagens.model.Pacote;
import com.fazmais.aluraviagens.ui.OnItemClickListiner;
import com.fazmais.aluraviagens.util.DiasUtil;
import com.fazmais.aluraviagens.util.MoedaUtil;
import com.fazmais.aluraviagens.util.ResourcesUtil;

import java.util.List;

public class ListaPacoteAdapterRecyclerView extends RecyclerView.Adapter<ListaPacoteAdapterRecyclerView.ListaPacoteViewHolder> {
    private final Context context;
    private final List<Pacote> pacotes;
    private OnItemClickListiner onItemClickListiner;

    public ListaPacoteAdapterRecyclerView(Context context, List<Pacote> pacotes) {
        this.context = context;
        this.pacotes = pacotes;
    }

    public void setOnItemClickListiner(OnItemClickListiner onItemClickListiner) {
        this.onItemClickListiner = onItemClickListiner;
    }

    @NonNull
    @Override
    public ListaPacoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viwCriada = LayoutInflater.from(context).inflate(R.layout.item_pacote, parent, false);
        return new ListaPacoteViewHolder(viwCriada);
    }

    @Override
    public void onBindViewHolder(ListaPacoteViewHolder holder, int position) {
        Pacote pacote = pacotes.get(position);
        holder.vincula(pacote);
    }

    @Override
    public int getItemCount() {
        return pacotes.size();
    }

    class ListaPacoteViewHolder extends RecyclerView.ViewHolder {

        private Pacote pacote;

        public ListaPacoteViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> onItemClickListiner.onItemClick(getLayoutPosition()));
        }

        public void vincula(Pacote pacote) {
            this.pacote = pacote;
            mostraLocal(pacote);
            mostraDias(pacote);
            mostraImagem(pacote);
            mostraPreço(pacote);
        }

        private void mostraPreço(Pacote pacote) {
            TextView preço = itemView.findViewById(R.id.item_pacote_preco);
            String formataMoedaParaBrasileiro = MoedaUtil.formataMoedaParaBrasileiro(pacote.getPreco());
            preço.setText(formataMoedaParaBrasileiro);
        }

        private void mostraImagem(Pacote pacote) {
            ImageView imagem = itemView.findViewById(R.id.item_pacote_imagem);
            Drawable drawable = ResourcesUtil.devolveDrawable(pacote.getImagem(), context);
            imagem.setImageDrawable(drawable);
        }

        private void mostraDias(Pacote pacote) {
            TextView dias = itemView.findViewById(R.id.item_pacote_dias);
            String diasEmTexto = DiasUtil.formtaDiasEmTexto(pacote.getDias(), context);
            dias.setText(diasEmTexto);
        }

        private void mostraLocal(Pacote pacote) {
            TextView local = itemView.findViewById(R.id.item_pacote_local);
            local.setText(pacote.getLocal());
        }
    }
}
