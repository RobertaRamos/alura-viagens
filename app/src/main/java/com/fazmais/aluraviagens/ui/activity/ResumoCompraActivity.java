package com.fazmais.aluraviagens.ui.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.fazmais.aluraviagens.R;
import com.fazmais.aluraviagens.model.Pacote;
import com.fazmais.aluraviagens.util.DataUtil;
import com.fazmais.aluraviagens.util.MoedaUtil;
import com.fazmais.aluraviagens.util.ResourcesUtil;

import static com.fazmais.aluraviagens.ui.activity.PacoteActivityConstantes.CHAVE_PACOTE;

public class ResumoCompraActivity extends AppCompatActivity {

    public static final String TITULO_APPBAR = "Resumo do pacoote";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumo_compra);
        setTitle(TITULO_APPBAR);
        carregaPacoteRecebido();
    }

    private void carregaPacoteRecebido() {
        Intent intent = getIntent();
        if (intent.hasExtra(CHAVE_PACOTE)) {
            Pacote pacote = (Pacote) intent.getSerializableExtra(CHAVE_PACOTE);
            inicializaCampos(pacote);

        }
    }

    private void inicializaCampos(Pacote pacote) {
        mostraImagem(pacote);
        mostraLocal(pacote);
        mostraData(pacote);
        mostraPreco(pacote);
    }

    private void mostraPreco(Pacote pacote) {
        TextView preco = findViewById(R.id.resumo_compra_preco);
        String precoFormatado = MoedaUtil.formataMoedaParaBrasileiro(pacote.getPreco());
        preco.setText(precoFormatado);
    }

    private void mostraData(Pacote pacote) {
        TextView data = findViewById(R.id.resumo_compra_data);
        String datatFormatada = DataUtil.periodoEmTexto(pacote.getDias());
        data.setText(datatFormatada);
    }

    private void mostraLocal(Pacote pacote) {
        TextView local = findViewById(R.id.resumo_compra_local);
        String localDoPacote = pacote.getLocal();
        local.setText(localDoPacote);
    }

    private void mostraImagem(Pacote pacote) {
        ImageView imagem = findViewById(R.id.resumo_compra_imagem_local);
        Drawable drawable = ResourcesUtil.devolveDrawable(pacote.getImagem(), this);
        imagem.setImageDrawable(drawable);
    }
}